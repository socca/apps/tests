#!/usr/bin/env python
# -*- coding: utf-8 -*-

# MIT License
#
# Copyright (c) 2018 BayLibre
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys

from coverage import Coverage
from pkg_resources import iter_entry_points
from unittest import TestResult

def run_tests():
    ret = 0
    for entrypoint in iter_entry_points('socca'):
        if entrypoint.name == 'run_tests':
            run_tests = entrypoint.load()
            res = run_tests(entrypoint.module_name)
            if isinstance(res, TestResult):
                if res.errors or res.failures:
                    ret = 1
    return ret

if __name__ == '__main__':
    omit = [
        '*pylink*',
        '*OpenOCD*',
        '*psutil*',
        '*pkg_resources*',
        '*lxml*',
    ]
    coverage = Coverage(omit=omit)
    coverage.erase()
    coverage.start()
    ret = run_tests()
    coverage.stop()
    coverage.save()
    coverage.html_report(omit=omit)
    sys.exit(ret)